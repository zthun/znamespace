# ZNamespace

ZNamespace is a very basic and rudimentary javascript library that makes it easy to create namespaces on the global window object.  

#### Installation

ZNamespace is installed using npm: 

```sh
$ npm install znamespace --save-dev
```

#### Build and Test

In order to build the module, you will need to install the following apps:

* [Node.js](https://nodejs.org/en/)

Once node is installed, you will need to add the grunt-cli to your global node repository:

```sh
$ npm install grunt-cli -g
```

If you want to check out and build the source code, you can do so from the bitbucket repository:

```sh
$ git clone https://bitbucket.org/zthun/znamespace
$ cd znamespace
$ npm install
$ grunt
```

#### Usage

You can include one of the following in your html file:

```sh
<!--Full Version.  Good for Debugging-->
<script src="node_modules/znamespace/dist/znamespace.js" />
```

```sh
<!--Minified Version.  Good for Production-->
<script src="node_modules/znamespace/dist/znamespace.min.js" />
```

If you are using NodeJS, you can actually import this into your node app by doing:

```sh 
var nsFactory = require('znamespace');  
```

Node doesn't really need namespaces due to its module structure, but this is availabe to you 
anyway in case you want it.  

In any javascript file, you can create a namespace by doing a path separated with the dot notation:

```
/*global znamespace*/

znamespace('zw.root.games');
zw.root.games.spaceshooter = {name: 'Spaceshooter'};
```

Likewise, in node, you can create namespaces as well by creating a global object:  

```
var appRoot = require('znamespace');
appRoot.znamespace('zw.root.games');
appRoot.zw.root.games.spaceshooter = {name: 'Spaceshooter'};
```

You can redeclare namespaces and it will not effect the namespace that is already in existance.  Doing a znamespace just guarentees that the namespace will be available for you when you need it.  

# License

### ISC License (ISC)
Copyright (c) 2017, Anthony Bonta

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.