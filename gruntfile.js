module.exports = function (grunt) {
    'use strict';
    
    require('load-grunt-tasks')(grunt);
    
    var pkg = grunt.file.readJSON('package.json');

    var fileList = {
        appScripts: [
            'lib/**/*.js',
            '!lib/**/*.spec.js'
        ],
        testScripts: [
            'lib/**/*.spec.js'
        ]
    };
    
    grunt.initConfig({
        // Configuration
        'paths': {
            coverage: 'coverage',
            build: 'dist',
            name: pkg.name
        },
        // Pre Processing 
        'clean': [
            '<%=paths.build %>',
            '<%=paths.coverage%>'
        ],
        // Checks
        'jshint': {
            options: {
                jshintrc: true
            },
            root: {
                src: ['*.js']
            },
            main: {
                src: fileList.appScripts
            },
            test: {
                src: fileList.testScripts
            }
        },
        'karma': {
            phantomjs: {
                configFile: 'karma.conf.js'
            }
        },
        'concat': {
            app: {
                src: fileList.appScripts,
                dest: '<%=paths.build%>/<%=paths.name%>.js'
            }
        },
        'uglify': {
            app: {
                src: '<%=paths.build%>/<%=paths.name%>.js',
                dest: '<%=paths.build%>/<%=paths.name%>.min.js'
            }
        },
    });
    
    grunt.registerTask('check', [
        'jshint',
        'karma'
    ]);
    
    grunt.registerTask('build', [
        'concat',
        'uglify'
    ]);
    
    grunt.registerTask('default', [
        'clean',
        'check',
        'build',
    ]);
};