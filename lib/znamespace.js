/* Note:  Because of the way the this pointer works in non-strict mode, 
 * in the browser, the root will be the window object.  In NodeJs, the 
 * root will be module.exports.znamespace.  You CAN use this in 
 * NodeJS if desired, but given how node uses a moduler system by 
 * default, it doesn't make a lot of sense to. 
 */
(function (root) {
    'use strict';
    
    root.znamespace = znamespace;
    
    /**
     * Represents a method to create a namespace on the global window object.
     * 
     * @param {String} nsName The name of the namespace to build.
     * 
     * @returns {Object} The end object that represents the final path in the namespace.
     */
    function znamespace(nsName) {
        var paths = nsName.split('.');
    
        for (var pathIndex = 0, start = root, len = paths.length; pathIndex < len; pathIndex += 1) {
            var currentPath = paths[pathIndex];
    
            if (!start[currentPath]) {
                start[currentPath] = { };
            }
            start = start[currentPath];
        }
    
        return start;
    }
})(this);