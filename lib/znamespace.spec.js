describe("Namespaces", function () {
    'use strict';

    it('should attach a pathed named to the global window object.', function () {
        // Arrange & Act
        window.znamespace('Foo.Bar.Happy');
        // Assert
        expect(window.Foo.Bar.Happy).toBeDefined();
    });
    it('should not overwrite an namespace that has already been declared.', function () {
        //Arrange
        var varData = 'Preserved';
        window.znamespace('Foo.Bar.Happy.Unique');
        window.Foo.Bar.Happy.Unique.MyVariable = varData;
        // Act
        window.znamespace('Foo.Bar.Happy.Unique');
        // Assert
        expect(window.Foo.Bar.Happy.Unique.MyVariable).toBe(varData);
    });
    
    it('returns the last path in the namespace.', function () {
        // Arrange & Act
        var result = window.znamespace('Foo.Bar.Happy.Unique');
        // Assert
        expect(result).toBe(window.Foo.Bar.Happy.Unique);
    });
});